import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramNavComponent } from './program-nav.component';

describe('ProgramNavComponent', () => {
  let component: ProgramNavComponent;
  let fixture: ComponentFixture<ProgramNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
