import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MemberService } from '../../services/api/member.service';
import { AuthService } from '../../services/auth/auth.service';
import { EventService } from '../../services/api/event.service';
import { SocketService } from '../../services/socket/socket.service';
import { FunctionService } from '../../services/function/function.service';
import { HistoryService } from '../../services/api/history.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  public user: any;
  public menus: any = [
    // { title: 'Welcome', router: '/about' },
    // { title: 'Program', router: '/program' },
    { title: 'LIVE', router: '/live' },
    { title: 'VOD', router: '/vod' },
    // { title: 'Speakers', router: '/speakers' },
    // { title: 'e-Posters', router: '/posters' },
    // { title: 'Sponsor', router: '/e-booth' },
    { title: 'NOTICE', router: '/board' },
  ];

  public isLive = false;
  public activeAbstractBtn: any = false;

  collapse = true;

  event;

  constructor(
    private router: Router,
    private eventService: EventService,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
    private functionService: FunctionService,
    private historyService: HistoryService,
  ) {
    router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((val) => {
        const auth = JSON.parse(sessionStorage.getItem('cfair'));
        this.user = auth && auth.token ? auth : undefined;
        if (this.user) {
          this.loginSocket();
        }

        $('.navbar-collapse').collapse('hide');
        this.collapse = true;

        this.functionService.scrollToTop();

        if (router.url === '/live') {
          this.isLive = true;
        } else {
          this.isLive = false;
        }
      });
  }

  ngOnInit(): void {
    // this.checkEventVersion();
    this.eventService.findOne().subscribe((res) => (this.event = res));
  }

  /** 초록집버튼 활성화여부 임시설정 */
  compareServerTime(): void {
    this.memberService.compareServerTime('2022-03-11 23:59:59').subscribe(res => {
      this.activeAbstractBtn = res;
    });
  }

  // 버전확인
  checkEventVersion(): void {
    this.eventService.findOne().subscribe((res) => {
      this.event = res;
      if (this.event.clientVersion) {
        const clientVersion = localStorage.getItem(
          this.event.eventCode + 'ver'
        );
        if (!clientVersion) {
          localStorage.setItem(
            this.event.eventCode + 'ver',
            this.event.clientVersion
          );
          location.reload();
        } else if (clientVersion) {
          if (clientVersion !== this.event.clientVersion) {
            localStorage.setItem(
              this.event.eventCode + 'ver',
              this.event.clientVersion
            );
            location.reload();
          }
        }
      }
    });
  }

  ngOnDestroy(): void { }

  postAttendance = (): void => {
    this.memberService.attendance(this.user.id).subscribe((res) => { });
  };

  // 유저데이터 제거
  private removeUser(): void {
    this.logoutSocket();
    sessionStorage.removeItem('cfair');
    this.user = null;
  }

  /** 로그아웃 */
  logout(): void {
    if (window.confirm('로그아웃 하시겠습니까?')) {
      const currentRoom = this.historyService.getRoom();
      const attendance = this.historyService.getAttendance();
      // 퇴장을 남겨야 할 경우만 동작
      if (currentRoom && attendance === 'out') {
        const options: {
          relationId: string;
          relationType: string;
          logType: string;
        } = {
          relationId: currentRoom.id,
          relationType: 'room',
          logType: this.historyService.getAttendance(),
        };

        this.memberService.history(this.user.id, options).subscribe(
          () => {
            this.historyService.setAttendance('in');
            this.router
              .navigate(['/login'])
              .catch(() => {
                this.removeUser();
              })
              .finally(() => {
                this.removeUser();
              });
          },
          (error) => {
            console.error(error);
            this.router
              .navigate(['/login'])
              .catch(() => {
                this.removeUser();
              })
              .finally(() => {
                this.removeUser();
              });
          }
        );
      } else {
        this.router
          .navigate(['/login'])
          .catch((error) => {
            this.removeUser();
          })
          .finally(() => {
            this.removeUser();
          });
      }
    }
  }

  loginSocket() {
    this.socketService.login({
      memberId: this.user.id,
      token: this.user.token,
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id,
    });
  }

  openKakao(): void {
    window.open('http://pf.kakao.com/_rxnSfs/chat', '_blank');
  }
}
