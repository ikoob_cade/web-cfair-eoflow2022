import * as _ from 'lodash';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { MemberService } from '../../services/api/member.service';
import { SocketService } from '../../services/socket/socket.service';
import { DateService } from '../../services/api/date.service';
declare var $: any;

enum Change_Password_Message {
  FILL_ALL = '모든 항목을 입력해 주세요.',
  FAILED_NEW_CONFIRM = '새로운 비밀번호 확인이 잘못 입력되었습니다.\n다시 확인해 주세요.',
  SUCCESS = '비밀번호가 변경되었습니다.',
  FAILED_OLD_PASSWORD = '비밀번호를 다시 확인해 주세요',
  SAME_PASSWORD = '기존 비밀번호와 새로운 비밀번호가 동일합니다',
  LENGTH = '새로운 비밀번호는 4자리 이상 입력해주세요'
}
@Component({
  selector: 'app-my-page',
  templateUrl: './my-page.component.html',
  styleUrls: ['./my-page.component.scss']
})
export class MyPageComponent implements OnInit, AfterViewInit {
  public user: any;
  public histories = [];
  public historiesOfVod: Array<any>;
  public boothsOfStamp: Array<any>[];
  public totalTime = null;

  // 시청 관련데이터
  public selectedHistory;
  public dates;
  public selectedLiveDateId: any = '';
  public selectedVodDateId: any = '';

  // 모달창 관련 데이터
  public modalTitle: String = '';
  public modalDate: String = '';

  // 비밀번호 변경
  public PASSWORD_OLD: string;
  public PASSWORD_NEW: string;
  public PASSWORD_CONFIRM: string;
  public PASSWORD_ERR: string = '';
  
  //에러메시지 위치 분기
  public errorSwitch = 0;

  constructor(
    public router: Router,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
    private activatedRoute: ActivatedRoute,
    private dateService: DateService
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));

    if (!this.user) {
      this.router.navigate(['/main']);
    }

    this.getDates();
    this.getMyInfo();
  }

  ngAfterViewInit(): void {
    this.checkIsAutoExit();
  }

  /** 날짜 목록 조회 */
  getDates(): void {
    this.dateService.find().subscribe(res => {
      this.dates = res;
    });
  }

  /** 사용자 정보 조회 */
  getMyInfo(): void {
    this.memberService.getMyInfo(this.user.id).subscribe(res => {
      if (res) {
        this.user = res;
      }
    });
  }

  /**
  * 자동퇴장 확인
  */
  checkIsAutoExit(): void {
    setTimeout(() => {
      this.activatedRoute.queryParams.subscribe(params => {
        if (params.isAutoExit === 'true') {
          alert('금일 행사가 종료되었습니다.\n로그아웃 해주시기 바랍니다.');
        }
      });
    }, 500);
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  /** 소켓 로그아웃 */
  logoutSocket(): void {
    this.socketService.logout({
      memberId: this.user.id
    });
  }

  /** LIVE 시청기록 조회 */
  getLiveRecoed() {
    this.modalTitle = 'LIVE';
    if (this.selectedLiveDateId) {
      this.memberService.getMyLiveHistory(this.user.id, this.selectedLiveDateId).subscribe(res => {
        this.modalDate = (res as any).date;
        this.histories = (res as any).data;
      })
      $('#recordModal').modal('show');
    }
  }
  /** VOD 시청기록 조회 */
  getVodRecord() {
    this.modalTitle = 'VOD';
    this.dates.forEach(element => {
      if(this.selectedVodDateId == element.id){
        this.modalDate = element.date
      }
    });
    if (this.selectedVodDateId) {
      this.memberService.getMyVodHistory(this.user.id, this.selectedVodDateId).subscribe(res => {
        this.histories = (res as any).logs;
      })
      $('#recordModal').modal('show');
    }
  }

  /** 비밀번호 변경 */
  changePassword(): void {
    const errorMessage = this.passwordValidator();
    if (!errorMessage) {
      this.memberService.changePassword(
        {
          password: this.PASSWORD_OLD,
          newPassword: this.PASSWORD_NEW,
          newPasswordConfirm: this.PASSWORD_CONFIRM
        }).subscribe(res => {
          this.errorSwitch = 0;
          alert('비밀번호 변경이 완료되었습니다');
          this.cancelChangePwd();
        }, err => { 
          this.errorSwitch = 1;
          this.PASSWORD_ERR = err.error.message;
        });

    } else {
      this.PASSWORD_ERR = errorMessage;
    }
  }

  /** 비밀번호 변경 취소 */
  cancelChangePwd(): void {
    $('#changePassword').modal('hide');
    this.PASSWORD_OLD = '';
    this.PASSWORD_NEW = '';
    this.PASSWORD_CONFIRM = '';
    this.PASSWORD_ERR = '';
  }

  /** 비밀번호 변경 유효성 검사 */
  passwordValidator(): string {
    if (!this.PASSWORD_NEW || !this.PASSWORD_CONFIRM || !this.PASSWORD_NEW) {
      this.errorSwitch = 3;
      return Change_Password_Message.FILL_ALL;
    } else if (this.PASSWORD_NEW.length < 4) {
      this.errorSwitch = 2;
      return Change_Password_Message.LENGTH;
    }else if (this.PASSWORD_NEW !== this.PASSWORD_CONFIRM) {
      this.errorSwitch = 3;
      return Change_Password_Message.FAILED_NEW_CONFIRM;
    } else if (this.PASSWORD_NEW == this.PASSWORD_OLD) {
      this.errorSwitch = 3;
      return Change_Password_Message.SAME_PASSWORD;
    }
    return null;
  }

}