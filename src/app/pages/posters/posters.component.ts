import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { PosterService } from '../../services/api/poster.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FunctionService } from '../../services/function/function.service';

@Component({
  selector: 'app-posters',
  templateUrl: './posters.component.html',
  styleUrls: ['./posters.component.scss']
})
export class PostersComponent implements OnInit {
  public allPosters: Array<any>; // 전체 포스터 목록
  public allCategories: Array<any>; // 전체 카테고리 목록
  public posters: Array<any>; // 포스터 목록
  public searchText: string; // 검색어
  public searchType: string; // 검색 카테고리

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private posterService: PosterService,
    private functionService: FunctionService
  ) {
    this.searchType = '';
  }

  public curCategory = 0;
  public filteredPosters // 순서값으로 후 가공된 전체 포스터목록

  ngOnInit(): void {
    this.loadPosters();
  }

  setCategory(categoryIndex): void {
    this.curCategory = categoryIndex;
    this.posters = _.filter(this.filteredPosters, poster => {
      return this.allCategories[this.curCategory].id === poster.categoryId;
    });
  }

  /**
   * 포스터 목록 조회
   */
  loadPosters = () => {
    this.posterService.find('', false).subscribe(res => {
      this.filteredPosters = _.sortBy(res, 'seq');
      this.allCategories = _.uniqBy(_.map(this.filteredPosters, 'category'), 'id');

      this.posters = _.filter(this.filteredPosters, poster => {
        return this.allCategories[this.curCategory].id === poster.categoryId;
      });
    });
  }

  /**
   * 포스터 목록을 카테고리 별로 정제한다.
   * @param posters 포스터 목록
   */
  sortByCategory = (posters) => {
    return _.chain(posters)
      .groupBy(poster => {
        return poster.category ? JSON.stringify(poster.category) : '{}';
      })
      .map((poster, category) => {
        category = JSON.parse(category);
        category.posters = poster;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }

  /** 카테고리를 선택한다. */
  selectCategory = (category) => {
    if (!category) {
      this.posters = this.allPosters;
    } else {
      this.posters = _.filter(this.allPosters, poster => {
        return poster.category.categoryName === category;
      });
    }
  }

  /** 카테고리 검색 */
  search = () => {
    if (this.searchText) {
      this.posters = _.filter(this.posters, poster => {
        return (
          poster.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
          poster.subTitle.toLowerCase().includes(this.searchText.toLowerCase()) ||
          poster.author?.toLowerCase().includes(this.searchText.toLowerCase())
        );
      });
    } else {
      this.posters = _.filter(this.filteredPosters, poster => {
        return this.allCategories[this.curCategory].id === poster.categoryId;
      });
    }
  }

  goTop(): void {
    this.functionService.scrollToTop();
  }

  /** 포스터 상세로 페이지 이동 */
  goDetail(posterId: string): void {
    this.router.navigate([`posters/${posterId}`]);
  }
}
